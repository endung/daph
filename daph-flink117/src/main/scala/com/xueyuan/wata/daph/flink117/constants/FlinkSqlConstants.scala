package com.xueyuan.wata.daph.flink117.constants

object FlinkSqlConstants {
	val DIALECT_DEFAULT = "default"
	val DIALECT_HIVE = "hive"

	val NAME_DEFAULT_CATALOG= "default_catalog"
	val NAME_DEFAULT_DATABASE = "default_database"
}
