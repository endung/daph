package com.xueyuan.wata.daph.starter.flink117

import com.xueyuan.wata.daph.api.GlobalContext
import com.xueyuan.wata.daph.constants.GlobalConstants.DEFAULT_XML_LOG_XML
import com.xueyuan.wata.daph.core.JVMStorage
import com.xueyuan.wata.daph.core.execution.ExecutorConfig
import com.xueyuan.wata.daph.flink117.computer.FlinkStreamComputer
import com.xueyuan.wata.daph.tools.jsonloader.{DJson, DefaultJsonLoaderFactory}
import com.xueyuan.wata.daph.tools.{CliTool, DefaultExecutorFactory, LogTool}
import com.xueyuan.wata.daph.utils.JsonUtil
import org.apache.commons.lang3.StringUtils
import org.apache.logging.log4j.scala.Logging

object DaphFlink117Main extends Logging {
  def main(args: Array[String]): Unit = {
    val cli = CliTool.extractInput(args)

    val logXml = cli.daphUserDefinedParameters.getOrElse("logXml", DEFAULT_XML_LOG_XML)
    LogTool.setLogXml(logXml)

    val jsonPath = DJson(
      cli.daphJobJsonPath,
      cli.daphComputerJsonPath,
      cli.daphStorageJsonPath,
      cli.daphExecutorJsonPath
    )
    val jsonResult = if (StringUtils.isEmpty(cli.daphJsonStorageType)) {
      DefaultJsonLoaderFactory.getJsonLoader().loadAll(jsonPath)
    } else {
      DefaultJsonLoaderFactory.getJsonLoader(cli.daphJsonStorageType, cli.daphJsonStorageConfig)
        .loadAll(jsonPath)
    }
    val jobJson = jsonResult.job
    val computerJson = jsonResult.computer
    val executorJson = jsonResult.executor

    // 创建Computer
    val computer = new FlinkStreamComputer(computerJson)

    // 创建Storage
    val storage = new JVMStorage

    // 创建Executor
    val executor = if (!StringUtils.isEmpty(executorJson)) {
      val exeConfig = JsonUtil.defaultMapper.readValue(executorJson, classOf[ExecutorConfig])
      DefaultExecutorFactory.getExecutor(exeConfig)
    } else null

    val gc =
      if (executor != null) new GlobalContext(jobJson, computer, storage, executor)
      else new GlobalContext(jobJson, computer, storage)
    gc.execute()
    gc.stop()
  }
}
