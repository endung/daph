#!/bin/bash
# Usage
echo "
Usage:
sh ${DAPH_HOME}/bin/daph-jvm-spark3.sh -j job.json -c computer.json
"

SHORT_OPTS="s:,g:,j:,c:,e:,d:"
LONG_OPTS="st:,sc:,job:,computer:,executor:,dudps:"
ARGS=$(getopt --options $SHORT_OPTS \
  --longoptions $LONG_OPTS -- "$@" )
eval set -- "$ARGS"
while true;
do
    case $1 in
        -s|--st)
           ST=$2
           ;;
        -g|--sc)
           SC=$2
           ;;
        -j|--job)
           JOB=$2
           ;;
        -c|--computer)
           COMPUTER=$2
           ;;
        -e|--executor)
           EXECUTOR=$2
           ;;

        -d|--dudps)
           DUDPS=$2
           ;;
        --)
           break
           ;;
    esac
    shift
done

CPS="${DAPH_HOME}/jars/framework/*:${DAPH_HOME}/jars/framework/lib/*:${DAPH_HOME}/jars/computers/spark3/*:${DAPH_HOME}/jars/computers/spark3/lib/*:${DAPH_HOME}/jars/starters/daph-starter-spark3.jar"

cmd="
java -cp ""${CPS}"" \
com.xueyuan.wata.daph.starter.spark3.DaphSpark3Main \

"
if [ -n "$ST" ]; then
    cmd="$cmd -st $ST"
fi
if [ -n "$SC" ]; then
    cmd="$cmd -sc $SC"
fi
if [ -n "$JOB" ]; then
    cmd="$cmd -job $JOB"
fi
if [ -n "$COMPUTER" ]; then
    cmd="$cmd -computer $COMPUTER"
fi
if [ -n "$EXECUTOR" ]; then
    cmd="$cmd -executor $EXECUTOR"
fi
if [ -n "$DUDPS" ]; then
    cmd="$cmd -dudps $DUDPS"
fi
echo "$cmd"

$cmd