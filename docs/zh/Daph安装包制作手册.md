<!-- TOC -->
  * [哪些情况应当制作安装包](#哪些情况应当制作安装包)
  * [安装包结构](#安装包结构)
  * [安装包正规制作流程](#安装包正规制作流程)
    * [1、按需修改相关模块的pom.xml](#1按需修改相关模块的pomxml)
    * [2、maven-install daph-core/jvm/spark3/flink117工程到本地maven仓库](#2maven-install-daph-corejvmspark3flink117工程到本地maven仓库)
    * [3、maven-package daph-nodes/starters工程](#3maven-package-daph-nodesstarters工程)
    * [4、制作安装包](#4制作安装包)
  * [安装包快速制作流程](#安装包快速制作流程)
<!-- TOC -->

## 哪些情况应当制作安装包

- Daph发布新版本
- Daph发行版本中的依赖组件版本，与目标平台相关组件版本，或不一致，或不兼容，比如Daph1.0.0依赖的spark版本是spark3.5.1，目标平台依赖的spark版本是spark2.4.8

## 安装包结构

```
daph    DAPH_HOME目录
├─bin   启动脚本目录
  ├─daph.sh                总启动脚本
  ├─daph-flink117.sh       Daph-flink117启动脚本
  ├─daph-jvm.sh            Daph-jvm启动脚本
  ├─daph-jvm-flink117.sh   Daph-jvm-flink117启动脚本
  ├─daph-jvm-spark3.sh     Daph-jvm-spark3启动脚本
  ├─daph-spark3.sh         Daph-spark3启动脚本
├─conf  默认配置文件目录
  ├─executor.json          默认执行器配置文件
  ├─log4j2.xml             默认日志配置文件
  ├─node-dictionary.json   节点信息配置文件
├─examples  示例文件目录
├─jars  jar文件目录
  ├─computers  Daph计算器jar目录
    ├─flink117              Daph-flink117的jar目录
      ├─lib                 Daph-flink117的所有依赖jar目录
      ├─daph-flink117-*.jar Daph-flink117 jar
    ├─jvm                   Daph-jvm的jar目录
      ├─lib                 Daph-jvm的所有依赖jar目录
      ├─daph-jvm-*.jar      Daph-jvm jar
    ├─spark3                Daph-spark3的jar目录
      ├─lib                 Daph-spark3的所有依赖jar目录
      ├─daph-spark3-*.jar   Daph-spark3 jar
  ├─framework  Daph-core的jar目录
    ├─lib                   Daph-core的所有依赖jar目录
    ├─daph-core-*.jar       Daph-core jar
  ├─nodes  Daph节点jar目录
    ├─daph-node-jvm-httpresult.jar   Daph-jvm计算器对应的http节点jar
    ├─...
  ├─starters  Daph的启动jar目录
    ├─daph-starter-flink117.jar
    ├─daph-starter-jvm.jar
    ├─daph-starter-spark3.jar
```

## 安装包正规制作流程

### 1、按需修改相关模块的pom.xml

- 一般不要修改daph pom.xml与daph-core pom.xml。
- 一般不必修改daph-jvm pom.xml。
    - daph-jvm目前还没有做深入设计与实现，目前支持的节点只为控制rest api的执行顺序
    - 当然，daph-jvm架构已经搭建好，您可以根据需求，进行二次开发。
- 一般不用修改daph-spark3/flink117 pom.xml，可以根据您的需求修改，以便适配您的数据平台。
    - daph-spark3/flink117对数据源的连接功能，基本上是直接运用spark与flink各自提供的数据源连接功能
    - 相关依赖，与spark/flink，及各个数据库官网所述完全一致，在maven仓库中找到对应依赖，更新daph-spark3/flink117 pom.xml即可
    - 可能存在冲突等问题，需要您自行解决。
- 一般不用修改daph-node-spark3/flink117个别子工程中的pom.xml，可以根据您的需求修改，以便适配您的数据平台。
    - 目前所有的节点工程中，只有daph-node-spark3的es/hbase/http工程有少量依赖，可以根据您的需求，酌情修改。

### 2、maven-install daph-core/jvm/spark3/flink117工程到本地maven仓库

- install后，第3步才能正常执行
- install形成的是assembly压缩包，用于第4步，制作版本压缩包

### 3、maven-package daph-nodes/starters工程

- package形成的是shade包，用于第4步，制作版本压缩包

### 4、制作安装包

1. 新建daph-pkg目录到文件系统
2. 拷贝daph源码工程中的bin/conf/examples目录到pkg目录
3. 新建jars目录到pkg目录
4. 新建computers/framework/nodes/starters目录到jars目录
5. 新建jvm/spark3/flink117目录到computers目录
6. 将daph-jvm/spark3/flink117源码工程的各自target下的tar.gz压缩包中的内容，分别解压到computers对应目录
7. 将daph-core源码工程的target下的tar.gz压缩包中的内容，解压到framework目录
8. 将daph-nodes源码工程下的各自子工程的target下的节点jar包，拷贝到nodes目录
9. 将daph-starters源码工程的target下的jar包，拷贝到starters目录
10. 重命名nodes/starters目录下的所有jar包，重命名规则是去掉版本号，比如daph-starter-jvm-1.0.0.jar，重命名为daph-starter-jvm.jar
11. 压缩daph-pkg目录

Done!

当然

- 若只是修改某个工程的源码或pom.xml，则可以只替换相关jar即可。比如，只修改了daph-spark3源码工程，则只需要将target下的tar.gz压缩包中的变更jars，替换到computers/spark3目录即可。
- 得益于Daph Framework与Computers均是assembly包，因此，若在服务器上安装部署Daph后，只需要替换某些jar包，则只需要从maven仓库下载jar包，随后将期望jar包上传到服务器上即可。

## 安装包快速制作流程

可基于Daph的最小化安装包，快速制作Daph安装包。<br>
Daph的最小化安装包包含在发行版本包中。

最小化安装包与正式安装包的区别是：

- 最小化安装包只有daph-jvm计算器可正常使用，可体验daph的DAG数据流能力
- 最小化安装包不包含daph-flink117与daph-spark3 pom.xml中的所有jar包
- 最小化安装包不包含daph-node-spark3-dataframe-batch-connector-hbase节点jar包

基于最小化安装包，将以上不包含的jar包，根据自身数据源连接需求，全部或部分分别放在特定目录，就可以完成Daph安装包的制作

- daph-flink117与daph-spark3
  pom.xml中的jar包，除core与log相关jar包外，根据需求，从maven仓库下载全部或部分，分别放在DAPH_HOME/computers/flink117/lib与DAPH_HOME/computers/spark3/lib目录下
-
打daph-node-spark3-dataframe-batch-connector-hbase节点jar包，重命名为daph-node-spark3-dataframe-batch-connector-hbase.jar，放在DAPH_HOME/nodes目录下（可选）
