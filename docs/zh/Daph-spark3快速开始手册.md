<!-- TOC -->
  * [准备](#准备)
  * [使用](#使用)
  * [job说明](#job说明)
    * [job DAG图](#job-dag图)
    * [job.json](#jobjson)
    <!-- TOC -->

## 准备

准备可用的spark3集群环境

## 使用

1. 将daph安装包解压到spark集群任一服务器节点的任意目录
2. 配置daph环境变量DAPH_HOME
3. 将${DAPH_HOME}/jars/computers/spark3目录及子目录内的jar包，根据需求，拷贝到${SPARK_HOME}/jars目录
4. 参照${DAPH_HOME}/examples/spark3/中的json文件，编写job.json与computer.json
5. 运行以下命令启动daph-spark3任务：

```shell
sh ${DAPH_HOME}/bin/daph.sh -a spark3 \
-j ${DAPH_HOME}/examples/spark3/job.json \
-c ${DAPH_HOME}/examples/spark3/computer.json
```

## job说明

### job DAG图

```mermaid
graph LR
    a[es-in] --> aa[hive-out];
```

### job.json

```json
{
  "nodes": [
    {
      "flag": "Spark3.dataframe.batch.connector.CommonInput",
      "config": {
        "format": "es",
        "cfg": {
          "es.nodes": "127.0.0.1:9200,127.0.0.2:9200"
        }
      },
      "outLines": [
        "in-line"
      ]
    },
    {
      "flag": "Spark3.dataframe.batch.connector.HiveOutput",
      "config": {
        "tableName": "t",
        "sql": "select * from t"
      },
      "inLines": [
        "in-line"
      ]
    }
  ]
}
```