<!-- TOC -->
  * [概述](#概述)
  * [准备](#准备)
    * [搭建运行环境](#搭建运行环境)
    * [配置环境变量](#配置环境变量)
  * [获取安装包](#获取安装包)
  * [安装部署](#安装部署)
  * [验证](#验证)
  * [常见问题与解决方案](#常见问题与解决方案)
<!-- TOC -->

## 概述

Daph的安装部署非常简单。<br>
比如安装部署daph-spark3：<br>

1. 安装daph-spark3，就是将daph安装包内的spark相关文件放到集群某个目录下
2. 部署daph-spark3，本质上就是部署spark应用。<br>
   因此，安装部署spark的所有方式，都适用于安装部署daph-spark3，比如命令行方式、DolphinScheduler、Azkaban，等等。

## 准备

### 搭建运行环境

Daph基于Java平台，因此需要Java环境。<br>
Daph进行实际数据处理，需要底层数据计算引擎，因此需要底层数据计算引擎环境。<br>
依赖版本如下：

| 元素    | 版本                         |
|-------|----------------------------|
| Java  | 1.8                        |
| Scala | 2.12                       |
| Spark | daph-spark3依赖spark3.5.1    |
| Flink | daph-flink117依赖flink1.17.2 |

兼容以上的依赖版本即可。

### 配置环境变量

配置**DAPH_HOME**

## 获取安装包

目前需根据[Daph安装包制作手册](Daph安装包制作手册.md)制作安装包。

## 安装部署

以命令行方式安装部署daph-spark3为例：

1. 解压daph安装包到spark集群涉及到的服务器节点的期望目录
2. 配置daph环境变量DAPH_HOME
3. 若不需要daph-jvm与daph-flink，可以删除daph目录内相关文件
4. 可选修改executor.json与log4j2.xml
5. 将${DAPH_HOME}/jars/computers/spark3目录及子目录中的所有jar包拷贝到$SPARK_HOME/jars目录

## 验证

1. 参照examples中的json文件，编写job.json与computer.json，可选编写executor.json与log4j2.xml
2. 运行以下命令启动daph-spark3任务：

```shell
sh ${DAPH_HOME}/bin/daph.sh -a spark3 -s local-fs \
-j ${DAPH_HOME}/examples/spark3/job-s.json \
-c ${DAPH_HOME}/examples/spark3/computer.json \
-e ${DAPH_HOME}/examples/spark3/executor.json \
-d ${DAPH_HOME}/examples/spark3/log4j2-spark3.xml
```

## 常见问题与解决方案

1）Daph core依赖scala2.12，因此若平台上的Spark/Flink是2.11，则需要将2.11版本的jars替换成2.12版本。<br>
2）Daph计算器依赖特定版本的jars，比如daph-spark3依赖spark3.5.1。因此，当平台版本与Daph计算器不匹配时，有两个选择，分别是

- 自行增删改daph源码中的pom.xml中的依赖jar为期望版本，编译打包，以适应自己的平台环境；
- 将平台环境中相应jar的版本，修改为Daph计算器的兼容版本。

3）Daph节点依赖特定版本的jars，需要根据自己的平台环境，替换相关jars。

- Daph节点jar是以shade方式打包。Daph节点jar对相关jars的依赖方式分为直接依赖和间接依赖：
- **直接依赖jars**指的是节点jar的类中直接引用的jars。
    - 若需要替换这部分jars，必须在节点源码工程中修改pom.xml，重新打包，才能完成替换。
- **间接依赖jars**指的是节点jar的类中没有直接引用的jars，比如daph-spark CommonIO节点，所需的jars实际上是spark
  datasources通过spi动态引入的jars。
    - 若需要替换这部分jars，因daph计算器都是以assembly方式打包，因此，只需要将期望的jars放入计算器目录，或底层计算引擎特定目录即可。