<!-- TOC -->
  * [连接器节点](#连接器节点)
    * [概况](#概况)
    * [支持的数据源](#支持的数据源)
    * [节点列表](#节点列表)
    * [Catalog](#catalog)
  * [转换器节点](#转换器节点)
    * [概况](#概况-1)
    * [节点列表](#节点列表-1)
<!-- TOC -->
## 连接器节点

### 概况

- 每个输入节点都可以流转一个表到多个下游节点
- 仅3种连接器节点，但已支持flink117所支持的所有数据源类型，详见**支持的数据源**
- 完美支持flink catalog

### 支持的数据源

- 支持的数据源类型，请参考[flink-docs-release-1.17/docs/connectors/table](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/table/overview/)。<br>
- 已支持的数据源类型相关的依赖jar版本，见[daph-flink117 pom.xml](../../../../../../daph-flink117/pom.xml)。<br>
- 可自行增删改pom.xml中的依赖jar版本，参照[Daph二次开发手册](../../../../../../docs/zh/Daph二次开发手册.md)
编译打包，参照[Daph安装包制作手册](../../../../../../docs/zh/Daph安装包制作手册.md)与[Daph安装部署手册](../../../../../../docs/zh/Daph安装部署手册.md)安装部署新打包的daph-flink117
jars，以适配自己的平台。

### 节点列表

| 支持的数据源类型 | 节点标识                                                    | 节点类型 | 节点功能                                    | 流批类型 | 节点说明书                                                                    |
|----------|---------------------------------------------------------|------|-----------------------------------------|------|--------------------------------------------------------------------------|
| 多种数据源    | Flink117.string.general.connector.GeneralInput          | 输入节点 | 可将一个数据表或文件读取成一个表，注册到TableEnv，并流转到多个下游节点 | 流批   | [节点说明书](connectors/String.general.connector.GeneralInput说明书.md)          |
| 多种数据源    | Flink117.string.general.connector.GeneralOutput         | 输出节点 | 将TableEnv中的一个表，写出到一个外部数据表或文件            | 流批   | [节点说明书](connectors/String.general.connector.GeneralOutput说明书.md)         |
| 多种数据源    | Flink117.string.general.connector.GeneralMultipleOutput | 输出节点 | 将TableEnv中的多个表，写出到多个外部数据表或文件            | 流批   | [节点说明书](connectors/String.general.connector.GeneralMultipleOutput说明书.md) |

### Catalog

- Daph-flink Catalog基于Flink Catalog，支持所有Flink Catalog。
- Daph-flink Catalog支持全局catalog与节点catalog。
    - 全局catalog是所有节点可见的catalog， 在computer.json文件中配置。
    - 节点catalog是某个节点可见的catalog，在job.json文件中配置。

## 转换器节点

### 概况

- 一个通用Sql节点

### 节点列表

| 输入输出数据结构              | 节点标识                                    | 节点类型   | 节点功能                                                    | 流批类型 | 节点说明书                                                      |
|-----------------------|-----------------------------------------|--------|---------------------------------------------------------|------|------------------------------------------------------------|
| 输入多个String，输出一个String | Flink117.string.general.transformer.Sql | 综合处理节点 | 基于截止上游节点时的TableEnv，执行sql，更新TableEnv，以供下游节点使用最新的TableEnv | 流批   | [节点说明书](transformers/String.general.transformer.Sql说明书.md) |
