<!-- TOC -->

* [简介](#简介)
* [配置项](#配置项)
    * [节点配置项](#节点配置项)
* [使用案例](#使用案例)
    * [DAG图](#dag图)
    * [job.json](#jobjson)

<!-- TOC -->

## 简介

- **节点标识**：Flink117.string.general.connector.GeneralMultipleOutput
- **简单标识**：flink117-sql-multipleout
- **节点类型**：输出节点
- **节点功能**：将TableEnv中的多个表，写出到多个外部数据表或文件
- **流批类型**：流批
- **支持的数据源类型**
  ：请参考[flink-docs-release-1.17/docs/connectors/table](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/table/overview/)

## 配置项

### 节点配置项

| 配置名称                | 配置类型               | 是否必填项 | 默认值             | 描述                                                                                                                                                                                                                                         |
|---------------------|--------------------|-------|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| catalogRangeType    | String             | 否     | default         | 若未配置createSqls与resultSqls，则必填。global：全局catalog<br/>local：本节点catalog<br/>default：flink默认catalog                                                                                                                                             |
| catalogDatabaseType | String             | 否     | -               | hive/jdbc/hudi/iceberg                                                                                                                                                                                                                     |
| catalogName         | String             | 否     | default_catalog | catalog名称                                                                                                                                                                                                                                  |
| catalogConfig       | Map[String,String] | 否     | -               | 请参考[flink-docs-release-1.17/docs/connectors/table](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/table/overview/)                                                                                             |
| databaseName        | String             | 否     | -               | catalog中某一个数据库名称。指定该参数后，下面的sqls应基于该数据库编写建表语句与插入语句                                                                                                                                                                                          |
| sqlDialect          | String             | 否     | default         | default/hive                                                                                                                                                                                                                               |
| createSqls          | Array[String]      | 否     | -               | 若未配置catalog，则必填。<br/>若未指定catalog或database，则createSql与resultSql中的表名格式必须是catalog名.数据库名.表名<br/>请参考[flink-docs-release-1.17/docs/connectors/table](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/table/overview/) |
| isOrder             | Boolean            | 否     | false           | 是否按照指定顺序执行resultSqls                                                                                                                                                                                                                       |
| resultSqls          | Array[OutSQL]      | 否     | -               | 若未配置catalog，则必填。<br/>若未指定catalog或database，则createSql与resultSql中的表名格式必须是catalog名.数据库名.表名<br/>请参考[flink-docs-release-1.17/docs/connectors/table](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/table/overview/) |

```scala
case class OutSQL(
  order: Int, // 顺序序号
  resultSql: String // Flink SQL表插入语句
)
```

## 使用案例

### DAG图

```mermaid
graph LR
    a1[mysql-in1] --> aa[mysql-out];
    a2[mysql-in2] --> aa[mysql-out];
```

### job.json

```json
{
  "nodes": [
    {
      "flag": "Flink117.string.general.connector.GeneralInput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "mysql_catalog",
        "databaseName": "wxy",
        "resultTableName": "in_t1"
      },
      "outLines": [
        "in-line1"
      ]
    },
    {
      "flag": "Flink117.string.general.connector.GeneralInput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "mysql_catalog",
        "databaseName": "wxy",
        "resultTableName": "in_t2"
      },
      "outLines": [
        "in-line2"
      ]
    },
    {
      "flag": "Flink117.string.general.connector.GeneralMultipleOutput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "mysql_catalog",
        "databaseName": "wxy",
        "resultSqls": [
          {
            "resultSql": "insert into in_t2 select * from in_t1 where f1 = '1'"
          },
          {
            "resultSql": "insert into in_t1 select * from in_t2 where f1 = '1'"
          }
        ]
      },
      "inLines": [
        "in-line1",
        "in-line2"
      ]
    }
  ]
}
```
