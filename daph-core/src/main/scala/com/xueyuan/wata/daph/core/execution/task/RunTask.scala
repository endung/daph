package com.xueyuan.wata.daph.core.execution.task

import com.xueyuan.wata.daph.api.GlobalContext
import com.xueyuan.wata.daph.api.node.NodeDescription

case class RunTask(nodeDescription: NodeDescription,
                   dc: GlobalContext) extends Task(nodeDescription, dc) {
  override def runTask(context: TaskContext): Unit = {
    node.runNode()
  }
}
