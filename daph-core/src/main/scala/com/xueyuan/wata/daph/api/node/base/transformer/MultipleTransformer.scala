package com.xueyuan.wata.daph.api.node.base.transformer

abstract class MultipleTransformer[IN, OUT] extends Transformer {
	final override def run(): Unit = {
		val be = before(lineToDS)
		val tr = transform(be)
		val af = after(tr)
		setDSToLines(af, nodeDescription.outLines)
	}

	protected def before(lineToDS: Map[String, IN]): Map[String, IN] = lineToDS
	protected def transform(lineToDS: Map[String, IN]): OUT
	protected def after(out: OUT): OUT = out

	final override def viewNode(): Unit = {
		val ds = view(lineToDS)
		setDSToLines(ds, Array(nodeDescription.id))
	}

	def view(laneToDS: Map[String, IN]): OUT = {
		transform(laneToDS)
	}

	private def lineToDS = getLineToDS.map { case (lane, ds) =>
		lane -> ds.asInstanceOf[IN]
	}
}
