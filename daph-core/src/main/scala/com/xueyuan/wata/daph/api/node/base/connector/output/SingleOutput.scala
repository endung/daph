package com.xueyuan.wata.daph.api.node.base.connector.output

import com.xueyuan.wata.daph.api.GlobalContext
import com.xueyuan.wata.daph.api.node.{NodeDescription, NodeInitErrorResult}

abstract class SingleOutput[IN] extends Output {
	override def init(stageDescription: NodeDescription,
                    gc: GlobalContext): java.util.List[NodeInitErrorResult] = {
		val res = super.init(stageDescription, gc)
		if (stageDescription.inLines.length > 1) {
			res.add(NodeInitErrorResult(
				this.getClass.getSimpleName,
				"inLines",
				s"${stageDescription.inLines.mkString("Array(", ", ", ")")}",
				"不应有多条输入线"
			))
		}
		res
	}
	
	final override def run(): Unit = {
		val res = before(ds)
		out(res)
		after()
	}

	protected def before(ds: IN): IN = ds
	protected def out(ds: IN): Unit
	protected def after(): Unit = {}

	final override def viewNode(): Unit = {
		val res = view(ds)
		setDSToLines(res, Array(nodeDescription.id))
	}

	def view(ds: IN): IN = {
		ds
	}

	private def ds = {
		val lines = nodeDescription.inLines

		if (lines.nonEmpty) getDSByLine(lines.head).asInstanceOf[IN]
		else null.asInstanceOf[IN]
	}
}
