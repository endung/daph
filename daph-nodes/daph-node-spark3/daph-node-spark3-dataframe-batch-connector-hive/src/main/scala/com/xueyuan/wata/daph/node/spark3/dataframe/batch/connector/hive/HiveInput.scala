package com.xueyuan.wata.daph.node.spark3.dataframe.batch.connector.hive

import com.xueyuan.wata.daph.spark3.api.node.dataframe.connector.input.DataFrameInput
import org.apache.spark.sql.DataFrame

class HiveInput extends DataFrameInput {
  override protected def in(): DataFrame = {
    val config = nodeConfig.asInstanceOf[HiveInputConfig]
    val sql = config.sql

    spark.sql(sql)
  }

  override def getNodeConfigClass = classOf[HiveInputConfig]
}