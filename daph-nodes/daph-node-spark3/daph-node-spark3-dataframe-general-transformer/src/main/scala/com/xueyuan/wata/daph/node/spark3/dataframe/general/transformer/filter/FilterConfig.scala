package com.xueyuan.wata.daph.node.spark3.dataframe.general.transformer.filter

import com.xueyuan.wata.daph.api.config.NodeConfig

case class FilterConfig(
  conditions: Array[Condition]
) extends NodeConfig

case class Condition(expression: String, isNot: Boolean = false)

