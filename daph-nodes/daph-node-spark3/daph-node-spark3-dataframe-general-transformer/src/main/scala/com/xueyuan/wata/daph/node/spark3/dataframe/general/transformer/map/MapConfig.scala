package com.xueyuan.wata.daph.node.spark3.dataframe.general.transformer.map

import com.xueyuan.wata.daph.api.config.NodeConfig

case class MapConfig(
  expressions: Array[String]
) extends NodeConfig