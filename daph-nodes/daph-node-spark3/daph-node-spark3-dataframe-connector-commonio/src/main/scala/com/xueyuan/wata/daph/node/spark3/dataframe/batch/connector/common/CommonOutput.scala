package com.xueyuan.wata.daph.node.spark3.dataframe.batch.connector.common

import com.xueyuan.wata.daph.node.spark3.dataframe.batch.connector.common.common.outDF
import com.xueyuan.wata.daph.spark3.api.node.dataframe.connector.output.DataFrameSingleOutput
import org.apache.spark.sql.DataFrame

class CommonOutput extends DataFrameSingleOutput {
  override def out(df: DataFrame): Unit = {
    val config = nodeConfig.asInstanceOf[CommonOutputConfig]

    outDF(df, config)
  }


  override def getNodeConfigClass = classOf[CommonOutputConfig]
}
