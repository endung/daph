package com.xueyuan.wata.daph.node.jvm.httpresult.simple.connector

import com.xueyuan.wata.daph.jvm.api.node.httpresult.{HttpResult, HttpResultInput}
import com.xueyuan.wata.daph.node.jvm.httpresult.{HttpConfig, http}

class HttpInput extends HttpResultInput {
  override protected def in(): HttpResult = {
    val config = nodeConfig.asInstanceOf[HttpConfig]

    val res = http(config)

    val message = res.message
    val code = res.code
    logger.info(s"Node[$id]Http success! Code is $code")

    res
  }

  override def getNodeConfigClass = classOf[HttpConfig]
}
