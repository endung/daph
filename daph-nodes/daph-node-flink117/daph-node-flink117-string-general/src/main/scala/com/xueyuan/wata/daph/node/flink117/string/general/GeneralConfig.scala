package com.xueyuan.wata.daph.node.flink117.string.general

import com.xueyuan.wata.daph.api.config.NodeConfig

class GeneralConfig(
  val catalogRangeType: String = "default",
  val catalogDatabaseType: String,
  val catalogName: String = "default_catalog",
  val catalogConfig: Map[String, String] = Map.empty,
  val databaseName: String = "default_database",
  val sqlDialect: String = "default"
) extends NodeConfig

class GeneralInputConfig(
  catalogRangeType: String = "default",
  catalogDatabaseType: String,
  catalogName: String = "default_catalog",
  catalogConfig: Map[String, String] = Map.empty,
  databaseName: String = "default_database",
  sqlDialect: String = "default",
  val resultTableName: String,
  val createSql: String,
  val resultSql: String
) extends GeneralConfig(catalogRangeType, catalogDatabaseType, catalogName, catalogConfig, databaseName, sqlDialect)

class GeneralOutputConfig(
  catalogRangeType: String = "default",
  catalogDatabaseType: String,
  catalogName: String = "default_catalog",
  catalogConfig: Map[String, String] = Map.empty,
  databaseName: String = "default_database",
  sqlDialect: String = "default",
  val createSql: String,
  val resultSql: String
) extends GeneralConfig(catalogRangeType, catalogDatabaseType, catalogName, catalogConfig, databaseName, sqlDialect)

class GeneralSQLConfig(
  val resultTableName: String,
  val sqlStatement: String
) extends NodeConfig

class GeneralMultipleOutputConfig(
  catalogRangeType: String = "default",
  catalogDatabaseType: String,
  catalogName: String = "default_catalog",
  catalogConfig: Map[String, String] = Map.empty,
  databaseName: String,
  sqlDialect: String = "default",
  val order: Boolean = false,
  val sqls: Array[OutSQL]
) extends GeneralConfig(catalogRangeType, catalogDatabaseType, catalogName, catalogConfig, databaseName, sqlDialect)

case class OutSQL(
  order: Int,
  createSql: String,
  resultSql: String
)
