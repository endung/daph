package com.xueyuan.wata.daph.node.flink117.string.general.connector

import com.xueyuan.wata.daph.flink117.api.node.string.StringSingleOutput
import com.xueyuan.wata.daph.node.flink117.string.general.{GeneralNode, GeneralOutputConfig}

class GeneralOutput extends StringSingleOutput with GeneralNode {
  override def out(inTableName: String): Unit = {
    val config = nodeConfig.asInstanceOf[GeneralOutputConfig]
    val databaseName = config.databaseName
    val createSql = config.createSql
    val resultSql = config.resultSql

    // 设置SQL方言，使用catalog
    su(config)

    // 创建表
    createTable(databaseName, createSql, resultSql)

    // 执行插入语句，打印执行结果
    val res = tableEnv.executeSql(resultSql)
    res.print()
  }

  override def getNodeConfigClass = classOf[GeneralOutputConfig]
}
