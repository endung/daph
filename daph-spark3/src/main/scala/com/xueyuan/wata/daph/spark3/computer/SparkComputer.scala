package com.xueyuan.wata.daph.spark3.computer

import com.xueyuan.wata.daph.api.computer.Computer
import com.xueyuan.wata.daph.spark3.utils.CommonUtils
import com.xueyuan.wata.daph.utils.JsonUtil.defaultMapper
import org.apache.spark.sql.SparkSession

class SparkComputer(config: SparkComputerConfig) extends Computer {
  override val entrypoint: SparkSession = CommonUtils.createSpark(config.envConfig)

  def this(json: String) = this(
    defaultMapper.readValue(json, classOf[SparkComputerConfig])
  )

  def this(map: Map[String, String]) = this(
    SparkComputerConfig(map, Map.empty)
  )

  override def stop(): Unit = entrypoint.stop()

  override def clear(): Unit = {
    entrypoint.catalog.clearCache()
    entrypoint.sparkContext.getPersistentRDDs.values.foreach(x => {
      x.unpersist(false)
    })
  }


}