package com.xueyuan.wata.daph.jvm.api.node.list

import com.xueyuan.wata.daph.api.node.base.connector.input.Input

abstract class ListInput[OUT]
  extends Input[List[OUT]]
