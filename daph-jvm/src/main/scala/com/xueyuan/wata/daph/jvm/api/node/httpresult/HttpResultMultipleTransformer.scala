package com.xueyuan.wata.daph.jvm.api.node.httpresult

import com.xueyuan.wata.daph.api.node.base.transformer.MultipleTransformer

abstract class HttpResultMultipleTransformer
  extends MultipleTransformer[HttpResult, HttpResult]
