package com.xueyuan.wata.daph.jvm.api.node.bool

import com.xueyuan.wata.daph.api.node.base.transformer.SingleTransformer

abstract class BoolSingleTransformer
  extends SingleTransformer[Boolean, Boolean]
