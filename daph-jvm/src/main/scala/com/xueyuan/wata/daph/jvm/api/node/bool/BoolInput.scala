package com.xueyuan.wata.daph.jvm.api.node.bool

import com.xueyuan.wata.daph.api.node.base.connector.input.Input

abstract class BoolInput
  extends Input[Boolean]
