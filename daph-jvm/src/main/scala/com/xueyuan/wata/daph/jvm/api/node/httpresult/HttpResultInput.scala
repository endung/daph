package com.xueyuan.wata.daph.jvm.api.node.httpresult

import com.xueyuan.wata.daph.api.node.base.connector.input.Input

abstract class HttpResultInput
  extends Input[HttpResult]
