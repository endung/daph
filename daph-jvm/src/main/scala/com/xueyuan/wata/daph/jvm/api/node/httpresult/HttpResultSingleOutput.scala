package com.xueyuan.wata.daph.jvm.api.node.httpresult

import com.xueyuan.wata.daph.api.node.base.connector.output.SingleOutput

abstract class HttpResultSingleOutput
  extends SingleOutput[HttpResult]
